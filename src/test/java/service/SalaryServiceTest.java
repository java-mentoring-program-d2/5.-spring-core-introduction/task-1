package service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SalaryServiceTest {
    private SalaryService salaryService;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-test.xml");
        salaryService = (SalaryService) context.getBean("salaryService");
    }

    @Test
    public void applicationContext() {
        Assert.assertNotNull(salaryService);
        System.out.println("Test application context started!");
    }
}