package util;

import model.Employee;
import model.Position;
import model.Salary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RandomGenerator {
    private static final Random random = new Random();
    private static final List<String> names = Arrays.asList("Aaron", "Abraham", "Adam", "Adrian", "Aidan", "Alan", "Albert",
            "Alejandro", "Alex", "Alexander", "Alfred", "Andrew", "Angel", "Anthony", "Antonio", "Ashton", "Austin",
            "Cameron", "Carl", "Carlos", "Charles", "Christopher", "Cole", "Connor", "Caleb", "Carter", "Chase",
            "Christian", "Clifford", "Cody", "Colin", "Curtis", "Cyrus", "Gabriel", "Gavin", "Geoffrey", "George",
            "Gerld", "Gilbert", "Gordon", "Graham", "Gregory");
    private static final List<String> skills = Arrays.asList("Engineer", "Sales Manager", "Marketer", "Manager", "Accountant");

    public static Employee getRandomEmployee() {
        String name = names.get(random.nextInt(names.size()));
        int age = random.nextInt(40) + 20;
        String skill = skills.get(random.nextInt(skills.size()));
        int skillLevel = random.nextInt(80) + 20;
        int expectedSalaryLevel = random.nextInt(3000) + 7000 * skillLevel / 100;
        return new Employee(name, age, skill, skillLevel, expectedSalaryLevel);
    }

    private static Employee getRandomEmployeeWithSkill(String requiredSkill) {
        final Employee employee = getRandomEmployee();
        employee.setSkill(requiredSkill);
        return employee;
    }

    public static Position getRandomPosition() {
        String name = skills.get(random.nextInt(skills.size()));
        int requiredSkill = 20 + random.nextInt(40);
        Salary salary = getRandomSalary();
        return new Position(name, salary, requiredSkill);
    }

    public static Position getRandomPositionFor(Employee employee) {
        return new Position(employee.getSkill(), new Salary(employee.getExpectedSalaryMinimum() + 1),
                employee.getSkillLevel() - 1);
    }

    public static Salary getRandomSalary() {
        int money = random.nextInt(7000) + 3000;
        return new Salary(money);
    }

    public static Map<String, List<Position>> generateAvailablePositions(int amount) {
        final Map<String, List<Position>> map = new HashMap<>();
        for (int i = 0; i < amount; i++) {
            Position position = getRandomPosition();
            String skill = position.getName();
            if (!map.containsKey(skill)) {
                map.put(skill, new ArrayList<>());
            }
            map.get(skill).add(position);
        }
        return map;
    }

    public static Map<String, List<Employee>> generateEmployees(int amount) {
        final Map<String, List<Employee>> map = new HashMap<>();
        for (int i = 0; i < amount; i++) {
            Employee employee = getRandomEmployee();
            Position position = getRandomPositionFor(employee);
            employee.setPosition(position);
            position.setEmployee(employee);
            String skill = employee.getSkill();
            if (!map.containsKey(skill)) {
                map.put(skill, new ArrayList<>());
            }
            map.get(skill).add(employee);
        }
        return map;
    }

    public static List<Employee> generateCandidates(int numberOfCandidatesThisWeek) {
        final List<Employee> list = new ArrayList<>();
        for (int j = 0; j < numberOfCandidatesThisWeek; j++) {
            list.add(getRandomEmployee());
        }
        return list;
    }

    public static Random getRandom() {
        return random;
    }

    public static List<String> getNames() {
        return names;
    }

    public static List<String> getSkills() {
        return skills;
    }
}
