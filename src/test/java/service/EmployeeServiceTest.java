package service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmployeeServiceTest {
    private EmployeeService employeeService;

    public EmployeeServiceTest() {
    }

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-test.xml");
        employeeService = (EmployeeService) context.getBean("employeeService");
    }

    @Test
    public void fireRandomlyAt() {
        Assert.assertNotNull(employeeService);
        System.out.println("Test application context started!");
    }

}