package model;

public class Position {
    private static int count = 1;
    private int id;
    private String name;
    private Salary salary;
    private int requiredSkill;
    private Employee employee;

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    public void setRequiredSkill(int requiredSkill) {
        this.requiredSkill = requiredSkill;
    }

    public int getId() {
        return id;
    }

    public Position(String name, Salary salary, int requiredSkill) {
        this.id = count++;
        this.name = name;
        this.salary = salary;
        this.requiredSkill = requiredSkill;
    }

    public Position() {
    }

    public String getName() {
        return name;
    }

    public Salary getSalary() {
        return salary;
    }

    public int getRequiredSkill() {
        return requiredSkill;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
